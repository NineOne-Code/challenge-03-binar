# Challenge 03 - Game System

## ERD

### [ERD dbdiagram.io](https://dbdiagram.io/d/624108d9bed618387309a740)

<img src="https://gitlab.com/NineOne-Code/challenge-03-binar/-/raw/master/challenge-03%20ERD.png" />


## DDL

### Create Database

```sql
CREATE DATABASE games;
```

### Create Tables

#### user_games
```sql
CREATE TABLE IF NOT EXISTS user_games (id SERIAL PRIMARY KEY, username VARCHAR(225), password VARCHAR(225));
```

#### user_game_histories
```sql
CREATE TABLE IF NOT EXISTS user_game_histories (id SERIAL PRIMARY KEY, time TIMESTAMP NOT NULL, 
score INT NOT NULL, user_id INT NOT NULL, CONSTRAINT fk_user_id_histories FOREIGN KEY(user_id) 
REFERENCES user_games(id) ON DELETE CASCADE)
```

#### user_game_biodatas

before create table, let's create ENUM for Gender:
```sql
CREATE TYPE Gender AS ENUM('m', 'f');
```

and then:
```sql
CREATE TABLE IF NOT EXISTS user_game_biodatas (id SERIAL PRIMARY KEY, name VARCHAR(225), age INT, 
gender Gender NOT NULL, level INT NOT NULL, user_id INT UNIQUE NOT NULL, CONSTRAINT fk_user_id_biodatas 
FOREIGN KEY(user_id) REFERENCES user_games(id) ON DELETE CASCADE);
```

## DML

### INSERT Data

#### user_games
```sql
INSERT INTO user_games (username, password) VALUES ('ibrahim', 'inipassword');
```

#### user_game_biodatas
```sql
INSERT INTO user_game_biodatas (name, age, gender, level, user_id) VALUES ('Ibrahim Nagib', 20, 'm', 0, 1);
```

#### user_game_histories
```sql
INSERT INTO user_game_histories (time, score, user_id) VALUES (CURRENT_TIMESTAMP, 100, 1);
```

### READ Data

#### user_games
```sql
SELECT * FROM user_games;
```

#### user_game_biodatas
```sql
SELECT * FROM user_game_biodatas;
```

#### user_game_histories
```sql
SELECT * FROM user_game_histories;
```

### UPDATE Data

```sql
UPDATE user_games SET username = '1br4h1m', password = 'passwordpanjaaannngggg' WHERE id = 1;
```

### DELETE Data

```sql
DELETE FROM user_games WHERE id = 2;
```