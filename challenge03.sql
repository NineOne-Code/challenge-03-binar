CREATE TABLE "user_games" (
  "id" int PRIMARY KEY,
  "username" varchar,
  "password" varchar
);

CREATE TABLE "user_game_biodatas" (
  "id" int PRIMARY KEY,
  "name" varchar,
  "age" int,
  "gender" enum,
  "level" int,
  "user_id" int
);

CREATE TABLE "user_game_hitsories" (
  "id" int PRIMARY KEY,
  "time" timestamp,
  "score" int,
  "user_id" int
);

ALTER TABLE "user_games" ADD FOREIGN KEY ("id") REFERENCES "user_game_biodatas" ("user_id");

ALTER TABLE "user_game_hitsories" ADD FOREIGN KEY ("user_id") REFERENCES "user_games" ("id");
